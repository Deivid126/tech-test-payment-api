﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;

namespace API.Models
{
  
    public class Venda
    {
        public int Id { get; set; }

        public DateTime data { get; set; }

        public string Status { get; set; }

        public List<string> itens { get; set; }

        public string cpf { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
    }
}
