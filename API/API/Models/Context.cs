﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Hosting;

namespace API.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        { }
        public DbSet<Venda> Vendas { get; set; }
        public Context()
            {
            }
      
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>(x =>
            {
                x.HasKey(y => y.Id);
                x.Property(y => y.itens)
                .HasConversion(
                    from => string.Join(";", from),
                    to => string.IsNullOrEmpty(to)
                     ? new List<string>()
                     : to.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList(),
                    new ValueComparer<List<string>>(
                        (c1, c2) => c1.SequenceEqual(c2),
                        c => c.Aggregate(0, (a, v) => HashCode.Combine(a, v.GetHashCode())),
                        c => c.ToList()
                        ));
           });
        }
    }
}
