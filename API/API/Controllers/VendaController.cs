﻿using API.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers

{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly Context _context;

        public VendaController(Context context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            venda.Status = "Aguardando Pagamento";
            if (venda.itens == null)
            {
                return BadRequest();
            }
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);


        }
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            else { return Ok(venda); }

        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda vendas)
        {
            var atualizarvenda = _context.Vendas.Find(id);

            if (atualizarvenda == null)
                return NotFound();

            atualizarvenda.Status = vendas.Status;


            _context.Vendas.Update(atualizarvenda);
            _context.SaveChanges();
            return Ok(atualizarvenda);
        }

        [HttpPatch("{AgurdandoparaAprovada}")]
        public IActionResult UpdateWaitingToApproved(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            venda.Status = "Pagamento Aprovado";
            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return Ok(venda);


        }

        [HttpPatch("{AguardandoparaCancelada}")]
        public IActionResult UpdateCancelada(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            venda.Status = "Cancelada";
            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return Ok(venda);


        }
        [HttpPatch("{AprovadaparaTrasnportadora}")]
        public IActionResult UpdateTrasnportadora(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            venda.Status = "Enviado para Transportadora";
            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return Ok(venda);


        }
        [HttpPatch("{TrasnpostadoraparaEntregue}")]
        public IActionResult UpdateTransEntregue(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            venda.Status = "Entregue";
            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return Ok(venda);


        }
        [HttpPatch("{AguardadndoraparaCancelada}")]
        public IActionResult UpdateAguardandoparaCancelada(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            venda.Status = "Cancelada";
            vendaBanco.Status = venda.Status;
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return Ok(venda);


        }





    }
}
